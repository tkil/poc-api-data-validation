import { ICharacter } from "types/index";
import { characterSchema } from "../../types/schema";

export const swapiDomain = "https://swapi.dev/api";

export const fetchCharacterByIdDataValidate = async (response: Response) => {
  try {
    const data: ICharacter = (await response.json()) as ICharacter;
    if (characterSchema.validate(data).length > 0) {
      return { data: undefined, status: "incomplete-data" };
    }
    return { data, status: "success" };
  } catch (err) {
    console.error("Error receiving and validating data");
    return { data: undefined, status: "error" };
  }
};

export const fetchCharacterById = async (id: number) => {
  try {
    const response = await fetch(`${swapiDomain}/people/${id}/`, {
      method: "get",
      headers: { "Content-Type": "application/json" },
    });
    return await fetchCharacterByIdDataValidate(response);
  } catch (err) {
    console.error(err);
    return { data: undefined, status: "error" };
  }
};
