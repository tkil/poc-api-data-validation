// Recomendation: Wire node-fetch into your jest setup
import fetch from "node-fetch";
//@ts-ignore
window.fetch = fetch;

// NPM
import nock from "nock";
// Project
import { fetchCharacterById, swapiDomain } from "../swapi";

describe("API swapi", () => {
  describe("mock API tests", () => {
    it("should have success status with dummy data", async () => {
      // Arrange
      const testPeopleId = 1;
      const mockResponse = {
        name: "Luke Skywalker",
        height: "172",
        mass: "77",
        hair_color: "blond",
        skin_color: "fair",
        eye_color: "blue",
        birth_year: "19BBY",
        gender: "male",
        homeworld: "http://swapi.dev/api/planets/1/",
        films: [
          "http://swapi.dev/api/films/1/",
          "http://swapi.dev/api/films/2/",
          "http://swapi.dev/api/films/3/",
          "http://swapi.dev/api/films/6/",
        ],
        species: [],
        vehicles: ["http://swapi.dev/api/vehicles/14/", "http://swapi.dev/api/vehicles/30/"],
        starships: ["http://swapi.dev/api/starships/12/", "http://swapi.dev/api/starships/22/"],
        created: "2014-12-09T13:50:51.644000Z",
        edited: "2014-12-20T21:17:56.891000Z",
        url: "http://swapi.dev/api/people/1/",
      };
      nock(swapiDomain).get(`/people/${testPeopleId}/`).reply(200, mockResponse);
      // Act
      const result = await fetchCharacterById(testPeopleId);
      // Assert
      expect(result.status).toEqual("success");
      expect(result.data).toEqual(mockResponse);
    });

    it("should have error status with undefined data", async () => {
      // Arrange
      const errorRestore = console.error;
      console.error = () => {};
      const testPeopleId = 9999;
      nock(swapiDomain).get(`/people/${testPeopleId}/`).reply(500);
      // Act
      const result = await fetchCharacterById(testPeopleId);
      // Assert
      expect(result.status).toEqual("error");
      expect(result.data).toBeUndefined();
      // Cleanup
      console.error = errorRestore;
    });

    it("should error on type validation", async () => {
      // Arrange
      const errorRestore = console.error;
      // console.error = () => {};
      const testPeopleId = 9999;
      const mockResponse = {
        name: "Luke Skywalker",
        height: "172",
        mass: "77",
      };
      nock(swapiDomain).get(`/people/${testPeopleId}/`).reply(200, mockResponse);
      // Act
      const result = await fetchCharacterById(testPeopleId);
      // Assert
      expect(result.status).toEqual("incomplete-data");
      expect(result.data).toBeUndefined();
      // Cleanup
      console.error = errorRestore;
    });
  });

  describe("live API tests", () => {
    beforeAll(() => {
      process.env.NOCK_OFF = "true";
    });
    afterAll(() => {
      process.env.NOCK_OFF = "false";
    });

    it("should fetch Luke", async () => {
      // Arrange
      const expected = {
        name: "Luke Skywalker",
        height: "172",
        mass: "77",
        hair_color: "blond",
        skin_color: "fair",
        eye_color: "blue",
        birth_year: "19BBY",
        gender: "male",
        homeworld: "http://swapi.dev/api/planets/1/",
        films: [
          "http://swapi.dev/api/films/1/",
          "http://swapi.dev/api/films/2/",
          "http://swapi.dev/api/films/3/",
          "http://swapi.dev/api/films/6/",
        ],
        species: [],
        vehicles: ["http://swapi.dev/api/vehicles/14/", "http://swapi.dev/api/vehicles/30/"],
        starships: ["http://swapi.dev/api/starships/12/", "http://swapi.dev/api/starships/22/"],
        created: "2014-12-09T13:50:51.644000Z",
        edited: "2014-12-20T21:17:56.891000Z",
        url: "http://swapi.dev/api/people/1/",
      };
      const testPeopleId = 1;
      // Act
      const result = await fetchCharacterById(testPeopleId);
      // Assert
      expect(result.status).toEqual("success");
      expect(result.data).toEqual(expected);
    });
  });
});
