# api-data-validation-poc
#### Repo: https://gitlab.com/tkil/api-data-validation-poc

### Summary
A POC that plays with validating rest API responses during runtime to guard against impure data.
The "good stuff" is in `./src/api/swapi.ts` and `./types/schema.ts`

### External Dependencies
* validate package

### Technologies Used
* Typescript

### Running Project
No app, just tests
```npm run test```

### Authors
* Tyler Kilburn <tylerkilburn@gmail.com>
