// Env
// const env = require('../../env/config')
// const envPath = env.envPath
// const envParsed = env.config().parsed

// Tools
const webpack = require("webpack");
const path = require("path");

// Plugins
// const CopyPlugin = require('copy-webpack-plugin')
// const Dotenv = require('dotenv-webpack')
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

// Variables - Optimization
const webpackMode = process.env.NODE_ENV === "production" ? "production" : "development";

// Variables - Directory Targets
const dir = path.resolve(__dirname);
const srcDir = path.join(dir, "src");
const distDir = path.join(dir, "dist");
const nodeModulesDir = path.join(dir, "node_modules");

// Variables - File Targets
const srcHtml = "index.html";
const srcIndex = "index.ts";
const distHtml = "index.html";
const distIndex = "index.js";

// Variables - Other
const devPort = process.env.DEV_SERVER_PORT;

// Main Webpack Config
module.exports = {
  mode: webpackMode,
  entry: path.join(srcDir, srcIndex),
  devServer: {
    compress: true,
    port: devPort,
    open: true,
    openPage: distHtml
  },
  module: {
    rules: [
      {
        test: /.(ts|tsx)$/,
        use: 'ts-loader',
        include: dir,
        exclude: [nodeModulesDir, /.test.tsx?$/]
      },
      {
        test: /\.(sa|sc|c)ss$/,
        include: dir,
        enforce: 'pre',
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: { importLoaders: 1, /*sourceMap: true,*/ url: false }
          },
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [
                require('autoprefixer')
              ]
            }
          }
        ]
      },
      {
        test: /\.(png|jp(e*)g|svg|ico)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 512,
              name: 'images/[hash]-[name].[ext]'
            }
          }
        ],
        exclude: nodeModulesDir
      }
    ]
  },
  output: {
    path: path.resolve(distDir),
    filename: '[name].[contenthash].js'
  },
  plugins: [
    // new Dotenv({
    //   path: envPath, // load this now instead of the ones in '.env'
    //   systemvars: true // load all the predefined 'process.env' variables which will trump anything local per dotenv specs.
    // }),
    // new webpack.DefinePlugin({
    //   ENV: JSON.stringify(envParsed)
    // }),
    new HtmlWebpackPlugin({
      // chunks: ['app'],
      filename: distHtml,
      // minify: { collapseWhitespace: true },
      template: path.join(srcDir, srcHtml)
    }),
    // new CopyPlugin({
    //   patterns: [
    //     { from: '', to: distDir },
    //   ]
    // }),
    new MiniCssExtractPlugin({
      filename: 'main.[contenthash].css',
      chunkFilename: '[name].[contenthash].css'
    })
  ],
  resolve: {
    mainFields: ['browser', 'main', 'module'],
    extensions: ['.ts', '.tsx', '.js', '.js', '.json'],
    plugins: [new TsconfigPathsPlugin({ configFile: path.join(dir, 'tsconfig.json') })]
  }
};
